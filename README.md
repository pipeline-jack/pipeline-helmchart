# DESAFIO Final - JackExpert
---
* Desafio Final - processo seletivo JackExpert
* Prática e desenvolvimento dos conceitos de helm em pipelines CI/CD
* O desafio consiste em utilizar uma esteira de CI/CD para deploy do nosso projeto desenvolvido durante as aulas no cluster disponibilizado pela jack.
---
# Histórico de Desenvolvimento
1. Conceitos CI/CD
    - [x] Conceitos 
    ---
2. Conhecer GitLab
    - [x] Conceitos básicos
    - [x] Entender runner
    - [x] Entender Git Agent

    - Foi usado um runner do foi criei no cluster "gitlab-runner" com uma imagem de ubunto, ele gerenciar o run da minha pipeline.
    - Foi configurado o Git Agent para permissões/integrações dentro do cluster.
    - Este runner cria containers dentro do cluster para executar cada job descrito no .gitlab-ci.yaml.
    - Utilização de images dentro deste container para solucionar necessidade de dependencias.
---
3. Conceitos da pipeline a se implantar
- Pipeline:
    - [x] build
        - Construir a image
        - Push na image
    - [x] deploy
        - habilitar o helm dentro do container criado para rodar minha pipeline
        - descodificar chave acesso ao cluster
        - upgrade/intall do meu helm chart
        